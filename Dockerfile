FROM node:9.3-alpine
LABEL Maintainer Filip Lauc <info@jaspero.co>
RUN apk add --no-cache \
			--repository http://dl-3.alpinelinux.org/alpine/edge/testing \
			vips-dev \
			fftw-dev \
			gcc \
			g++ \
			make \
			libc6-compat